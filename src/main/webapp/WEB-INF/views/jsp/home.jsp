<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="fragments/header.jsp" />

<body>

	<div class="container">

		<h2>Welcome!!!</h2>
		<br /> <br />



		<c:set var="feature1_label"
			value="Frames & Java Collection Framework" />
		<c:set var="feature2_label"
			value="Images, and Windows components" />
		<c:set var="feature3_label"
			value="Working with iFrames, Keywork Framework, Apache POI" />
		<c:set var="feature4_label"
			value="Working with Implicit Wait, Explicit Wait, Fluent Wait, Event Handling, Apache POI" />
		<c:set var="feature5_label" value="Working POM, Page Factory Model" />

		<c:set var="feature1" value="Frames" />
		<c:set var="feature2" value="Images" />
		<c:set var="feature3" value="iFrame + Keywork Framework + Apache POI" />
		<c:set var="feature4" value="Waits + Event Handling + Apache POI" />
		<c:set var="feature5" value="POM, Page Factory Model" />

		<table class="table table-striped">
			<tr>
				<td>
					<h4>${feature1_label}</h4>
				</td>
				<td align="right">

					<button class="btn btn-info" target="_blank"
						onclick="window.open('https://seleniumhq.github.io/selenium/docs/api/java/')">${feature1}</button>
				</td>
			</tr>
			<tr>
				<td>
					<h4>${feature2_label}</h4>
				</td>
				<td align="right">

					<button class="btn btn-info" onclick="location.href='images'">${feature2}</button>
				</td>
			</tr>
			<tr>
				<td>
					<h4>${feature3_label}</h4>
				</td>
				<td align="right">

					<button class="btn btn-info" onclick="location.href='iframe'">${feature3}</button>
				</td>
			</tr>
			<tr>
				<td>
					<h4>${feature4_label}</h4>
				</td>
				<td align="right">

					<button class="btn btn-info" onclick="location.href='iframe'">${feature4}</button>
				</td>
			</tr>
			<tr>
				<td>
					<h4>${feature5_label}</h4>
				</td>
				<td align="right">

					<button class="btn btn-info" onclick="location.href='iframe'">${feature5}</button>
				</td>
			</tr>

		</table>

	</div>

	<jsp:include page="fragments/footer.jsp" />

</body>
</html>