<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="fragments/header.jsp" />

<spring:url value="/resources/img/c2t-FrontPag.PNG" var="image1" />
<spring:url value="/resources/img/Capture.GIF" var="image2" />
<spring:url value="/resources/img/Coffee.jpg" var="image3" />

<body>

	<div class="container">

		<h2>iFrame!!!</h2>
		<br /> <br />

		<c:set var="feature1" value="Frames" />
		<c:set var="feature2" value="Pop Up" />

		<table class="table table-striped">
			<tr>
				<td height="200"><iframe name="iframe1" id="IF1" src="http://demoqa.com/" width="100%" height="200%"></iframe></td>
			</tr>
			<tr>
				<td height="200"><iframe name="iframe2" id="IF2" src="http://c2t.nchaurasia.in/blog/" width="100%" height="200%"></iframe></td>
			</tr>
		</table>

	</div>

	<jsp:include page="fragments/footer.jsp" />

</body>
</html>