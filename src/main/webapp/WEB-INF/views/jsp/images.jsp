<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="fragments/header.jsp" />

<spring:url value="/resources/img/c2t-FrontPag.PNG"
	var="image1" />
<spring:url value="/resources/img/Capture.GIF"
	var="image2" />
<spring:url value="/resources/img/Coffee.jpg"
	var="image3" />	
	
<body>

	<div class="container">

		<h2>Images!!!</h2>
		<br/>
		<br/>

		<c:set var="feature1" value="Frames" />
		<c:set var="feature2" value="Pop Up" />

		<table class="table table-striped">
			<tr>
				<td><img src="${image1}" width="250" height="250"/></td>
				<td><img src="${image2}" width="250" height="250"/></td>
				<td><img src="${image3}" width="250" height="250"/></td>
			</tr>
			<tr>
				<td><img src="${image2}" width="250" height="250"/></td>
				<td><img src="${image1}" width="250" height="250"/></td>
				<td><img src="${image3}" width="250" height="250"/></td>
			</tr>
		</table>

	</div>

	<jsp:include page="fragments/footer.jsp" />

</body>
</html>