package com.mkyong.form.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import javax.validation.Valid;

//http://www.tikalk.com/redirectattributes-new-feature-spring-mvc-31/
//https://en.wikipedia.org/wiki/Post/Redirect/Get
//http://www.oschina.net/translate/spring-mvc-flash-attribute-example
@Controller
public class GenericController {

	private final Logger logger = LoggerFactory.getLogger(GenericController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		logger.debug("index()");
		//return "redirect:/users";
		return "home";
	}
	
	@RequestMapping(value = "/images", method = RequestMethod.GET)
	public String images() {
		logger.debug("index()");
		//return "redirect:/users";
		return "images";
	}
	
	@RequestMapping(value = "/iframe", method = RequestMethod.GET)
	public String iframe() {
		logger.debug("index()");
		//return "redirect:/users";
		return "iframe";
	}

}